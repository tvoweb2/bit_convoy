#!/usr/local/bin/ruby
# -*- coding: utf-8 -*-

require "webrick"
module WEBrick::HTTPServlet
	FileHandler.add_handler('rb', CGIHandler)
end

server = WEBrick::HTTPServer.new({
	:BindAddress => "0.0.0.0",
	:Port => 3000,
	:DocumentRoot => "/opt/bit_convoy/http",
	:CGIInterpretter => "/usr/local/bin/ruby"
})

trap(:INT){server.shutdown}

server.start
