#!/usr/local/bin/ruby
# -*- coding: utf-8 -*-

require "cgi"
require "json"

cgi = CGI.new

begin
	require "../lib/yotta/yotta.rb"
	require "../lib/bit_convoy/bit_convoy.rb"

	cargo_type = cgi.params["cargo"][0]
	BitConvoy::Convoy.new.transport(cargo_type) unless cargo_type.nil?
rescue => exc
	e = exc
end

cgi.out({"type" => "application/json", "charset" => "UTF-8"}){
	if e.nil?
		JSON.generate({:success => true})
	else
		JSON.generate({:success => false, :err_mss => e})
	end
}
