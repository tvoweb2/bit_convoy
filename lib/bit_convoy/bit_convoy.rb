# -*- coding: utf-8 -*-

module BitConvoy
	require "tempfile"
	require "fileutils"
	require "inifile"
	require "net/http"
	require "uri"

	Config = IniFile.load "/etc/opt/tsweb/bit_convoy/bit_convoy.conf"

	def self.configure config
		self.Config = config
	end

	class Convoy
		def initialize
			config = Config["protocol"]
			raise "no config[protocol]" if config.nil?
			@protocol = ::Yotta::Protocol::Factory.new(config["type"]).create({
				:host => config["remote_host"],
				:port => config["remote_port"],
				:username => config["remote_username"],
				:keys => config["ssh_keys"],
				:passphrase => config["ssh_keys_passphrase"]
			})
		end

		def transport type
			@type = type
			config = Config[@type]
			raise "no config[@type]" if config.nil?
			cargo = Cargo::Factory.new(config["cargo_type"]).create(config)
			deploy(cargo)
		end

		private

		def deploy cargo
			raise "Convoy#deploy: cargo is nil." if cargo.nil?
			raise "Convoy#deploy: cargo.src is nil." if cargo.src.nil?
			raise "Convoy#deploy: cargo.dest is nil." if cargo.dest.nil?
			raise "Convoy#deploy: cargo's source file doesnt exist." if !::File.exist?(cargo.src)
			@protocol.deploy(cargo.src, cargo.dest)
		end
	end

	module Cargo
		class Factory < ::Yotta::Factory
			def create opts=nil
				case @type
				when "HTTP"
					HTTP.new opts
				end
			end
		end

		class Base
			attr_reader :src
			attr_reader :dest

			def initialize opts
				@src = !opts["src"].nil? ? opts["src"] : nil
				@dest = !opts["dest"].nil? ? opts["dest"] : nil
			end
		end

		class HTTP < Base
			def initialize opts
				raise "opts[src] isnt URI" unless /^https?\:\/\// =~ opts["src"]
				opts["src"] = tempfile(opts["src"])
				super opts
			end

			private

			def tempfile url
				if url.instance_of?(Array)
					url.map do |url|
						tempfile url
					end
				else
					return ::Tempfile.open("__bit_convoy_#{Time.now.to_i}#{Time.now.usec}_#{::File.basename(url)}") {|file|
						parsed = URI.parse(url)
						res = Net::HTTP.start(parsed.host, parsed.port) {|http|
							timeout = Config["general"]["http_timeout"].to_i
							http.read_timeout = timeout.nil? ? 120 : timeout
							req = Net::HTTP::Get.new(parsed.path + (parsed.query.nil? ? "":"?"+parsed.query))
							req.basic_auth parsed.user, parsed.password
							http.request(req)
						}
						if res.code == "200"
							file.puts res.body
							file.path
						end
					}
				end
			end
		end
	end
end
